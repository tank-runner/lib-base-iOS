import XCTest
@testable import LibBase

final class LibBaseTests: XCTestCase {
    
    func testStringEx() throws {
    
        XCTAssertEqual("1".ex.int, 1)
        
        XCTAssertEqual("1.0".ex.int, nil)
        
        XCTAssertEqual("a".ex.int, nil)
    
        XCTAssertEqual("1".ex.float, 1)
        
        XCTAssertEqual("2.0".ex.float, 2.0)
        
        XCTAssertEqual("2.0".ex.double, 2.0)
        
        XCTAssertEqual(String(100.0), "100.0")
    }
}
