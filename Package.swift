// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "LibBase",
    platforms: [
        .macOS(.v10_10), .iOS(.v12), .tvOS(.v9), .watchOS(.v3)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "LibBase",
            targets: ["LibBase"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(name: "Moya", url: "git@gitee.com:tank-runner/Moya.git", .exact("15.0.0-appfixed.2")),
        .package(name: "AlamofireImage", url: "git@gitee.com:tank-runner/AlamofireImage.git", .exact("4.2.0-appfixed.1")),
        .package(name: "DictionaryCoding", url: "git@gitee.com:tank-runner/DictionaryCoding.git", .upToNextMajor(from: "1.0.9")),
        .package(name: "RxSwift", url: "https://gitee.com/tank-runner/RxSwift.git", .upToNextMajor(from: "6.2.0")),
        .package(name: "LibLevelDB", path: "../LibLevelDB"),
        .package(name: "FileLogging", url: "https://github.com/crspybits/swift-log-file.git", .upToNextMajor(from: "0.1.0")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "LibBase",
            dependencies: [
                .product(name: "RxMoya", package: "Moya"),
                .product(name: "RxCocoa", package: "RxSwift"),
                "FileLogging",
                "DictionaryCoding",
                "AlamofireImage",
                "LibLevelDB"]
        ),
        .testTarget(
            name: "LibBaseTests",
            dependencies: ["LibBase"]),
    ]
)
