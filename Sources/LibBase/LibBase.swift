//
//  LibBase.swift
//  
//
//  Created by tank on 2023/7/20.
//

import Foundation

public class LibBase {
    static public let shared: LibBase = LibBase()
    
    private init() {
    }
    
    public var appPubKey: SecCertificate?
}
