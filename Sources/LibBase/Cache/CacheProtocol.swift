//
//  CacheProtocol.swift
//  
//
//  Created by tank on 2021/11/17.
//

import Foundation

public protocol CacheProtocol: AnyObject {

    var path: String? { get }

    init(path: String?)

    func cacheKey(key: String) -> String

    func bool(key: String) -> Bool?

    func int(key: String) -> Int?

    func float(key: String) -> Float?

    func double(key: String) -> Double?

    func string(key: String) -> String?
    
    func data(key: String) -> Data?
    
    func object<T: Codable>(type: T.Type, key: String) -> T?

    func objectNSSecureCoding<T: NSSecureCoding & NSObject>(type: T.Type, key: String) -> T?

    func setBool(value: Bool, key: String)

    func setInt(value: Int, key: String)

    func setFloat(value: Float, key: String)

    func setDouble(value: Double, key: String)

    func setString(value: String, key: String)
    
    func setData(value: Data, key: String)

    func setObject<T: Codable>(value: T, key: String)

    func setObjectNSSecureCoding<T: NSSecureCoding>(value: T, key: String)

    func remove(key: String)

    func removeAll()
}

extension CacheProtocol {

    public func cacheKey(key: String) -> String {
        guard let path = path, !path.isEmpty else {
            return "/\(key)"
        }

        return "/\(path)/\(key)"
    }
}
