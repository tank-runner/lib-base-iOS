//
//  WeakRef.swift
//  
//
//  Created by tank on 2021/10/9.
//

import Foundation

public struct WeakRef<T: AnyObject> {
    
    public weak var value: T?

    
    public init(value: T?) {
        self.value = value
    }
}
