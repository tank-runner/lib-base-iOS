//
//  FileManager+Ex.swift
//  
//
//  Created by 孙长坦 on 2022/5/3.
//

import Foundation

public extension Extension where Base == FileManager {
    static let documentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()
    
    static let cacheDirectory: URL = {
        let urls = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()
    
    static let downloadDirectory: URL = {
        let directory: URL = FileManager.ex.documentsDirectory.appendingPathComponent("Download")
        return directory
    }()
}
