//
//  Extension.swift
//  
//
//  Created by tank on 2021/9/29.
//

import Foundation

/// Extension
public struct Extension<Base> {

    /// Base object to extend.
    public let base: Base

    /// Create extension with base object.
    ///
    /// - parameter base: Base object.
    public init(_ base: Base) {
        self.base = base
    }
}

/// 持有Extension实例的协议
public protocol ExtensionCompatible {

    associatedtype ExtensionCompatibleType

    static var ex: Extension<ExtensionCompatibleType>.Type { get }

    var ex: Extension<ExtensionCompatibleType> { get }
}

public extension ExtensionCompatible {

    static var ex: Extension<Self>.Type {
        return Extension<Self>.self
    }

    var ex: Extension<Self> {
        return Extension(self)
    }
}
