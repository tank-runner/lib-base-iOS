//
//  Data+Ex.swift
//  
//
//  Created by 孙长坦 on 2023/3/15.
//

import Foundation

/// Extend Data with `ex` proxy.
extension Data: ExtensionCompatible {}

public extension Extension where Base == Data {
    func toHexString() -> String {
        return base.reduce("", {$0 + String(format: "%02x", $1)})
    }
}
