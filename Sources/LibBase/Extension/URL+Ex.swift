//
//  URL+Ex.swift
//  
//
//  Created by 孙长坦 on 2022/5/3.
//

import Foundation

/// Extend String with `ex` proxy.
extension URL: ExtensionCompatible {}

public extension Extension where Base == URL {
    func downloadLocation(fileName: String?) -> URL {
        let fileKey: String = base.absoluteString.ex.md5
        var filePath: URL = FileManager.ex.downloadDirectory
        filePath.appendPathComponent(fileKey)
        
        if let fileName = fileName {
            filePath.appendPathComponent(fileName)
        }
        
        return filePath
    }
}
