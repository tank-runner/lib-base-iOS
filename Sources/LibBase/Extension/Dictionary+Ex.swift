//
//  Dictionary+Ex.swift
//  
//
//  Created by 孙长坦 on 2021/12/30.
//

import Foundation

extension Dictionary: ExtensionCompatible {}

public extension Dictionary {
    
    static func merging(value1: [Key: Value]?, value2: [Key: Value]?) -> [Key: Value]? {
        guard let value1 = value1 else {
            return value2
        }
        
        guard let value2 = value2 else {
            return value1
        }
        
        return value1.merging(value2, uniquingKeysWith: { _, newValue in
            return newValue
        })        
    }
}
