//
//  Date+Ex.swift
//  
//
//  Created by 孙长坦 on 2023/3/13.
//

import Foundation

/// Extend Date with `ex` proxy.
extension Date: ExtensionCompatible {}

public extension Extension where Base == Date {
    var integerTimeIntervalSince1970: Int {
        return Int(base.timeIntervalSince1970 * 1000)
    }
}
