//
//  BehaviorRelay+Codable.swift
//  
//
//  Created by tank on 2023/8/29.
//

import UIKit
import RxRelay

// MARK: - Codable
extension BehaviorRelay: Codable where Element: Codable {
    convenience public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let value = try container.decode(Element.self)
        self.init(value: value)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(value)
    }

}
