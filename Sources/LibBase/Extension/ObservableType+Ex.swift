//
//  ObservableType+Ex.swift
//  
//
//  Created by tank on 2021/12/4.
//

import Foundation
import RxSwift

public struct CurAndPreData<T: Equatable> {
    let current: T
    let previous: T

    init(current: T, previous: T) {
        self.current = current
        self.previous = previous
    }
}

public extension ObservableType where Element: Equatable {
    func previous(first: Element) -> Observable<CurAndPreData<Element>> {
        return scan(CurAndPreData(current: first, previous: first), accumulator: { (seed, value) -> CurAndPreData<Element> in
            return CurAndPreData(current: value, previous: seed.current)
        })
    }

    func filterDuplicates(first: Element) -> Observable<Element> {
        return previous(first: first).filter { value in
            return value.current != value.previous
        }.map { value in
            return value.current
        }
    }
}
