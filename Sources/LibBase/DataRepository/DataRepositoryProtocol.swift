//
//  DataRepositoryProtocol.swift
//  
//
//  Created by 孙长坦 on 2022/3/28.
//

import Foundation

public protocol DataRepositoryProtocol: AnyObject {

    static var id: String { get }

    var dataChangeCallBack: ((_ encodeData: Any?) -> Void)? { get set }
    
    var itemChangeCallBack: ((_ encodeItem: Any?) -> Void)? { get set }
    
    init()
    
    func set(encodeData: Any?)

    func getEncodeData() -> Any?
    
    func save()
    
    func update(encodeItem: Any)
    
    func update(itemId: String, key: String, value: Any?)

    func add(encodeItem: Any)
    
    func insert(encodeItem: Any)

    func getEncodeItem(index: Int) -> Any?

    func getEncodeItem(itemId: String) -> Any?
    
    func removeItem(itemId: String)
}
