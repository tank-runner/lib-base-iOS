//
//  CodableBuilder.swift
//  
//
//  Created by 孙长坦 on 2022/3/28.
//

import Foundation
import DictionaryCoding

public class CodableBuilder {
    
    public init() {
        
    }
    
    public func createJSONEncoder() -> JSONEncoder {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .millisecondsSince1970
        
        return encoder
    }
 
    public func createJSONDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return decoder
    }
    
    public func createDictionaryEncoder() -> DictionaryEncoder {
        let encoder = DictionaryEncoder()
        encoder.dateEncodingStrategy = .millisecondsSince1970
        
        return encoder
    }
    
    public func createDictionaryDecoder() -> DictionaryDecoder {
        let decoder = DictionaryDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        return decoder
    }
}
